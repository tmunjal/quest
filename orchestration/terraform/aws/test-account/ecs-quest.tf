data "aws_caller_identity" "current" {}

resource "aws_ssm_parameter" "quest_secret" {
  name  = "/quest/secret"
  type  = "SecureString"
  value = random_password.quest_secret.result
  tags = {
    Name = "Secret for quest application"
  }
}

resource "random_password" "quest_secret" {
  length           = 16
  special          = true
  override_special = "_!%^"
}

resource "aws_cloudwatch_log_group" "cw_log_group" {
  name = "/ecs/quest"
  tags = { "Name" = "ecs cloudwatch" }
}

module "quest" {
  vpc_id                = module.infra.outputs["vpc_id"]
  ecs_cluster_name      = "quest"
  ecs_service_name      = "quest"
  source                = "../modules//ecs"
  alb_name              = "quest-ecs"
  alb_internal          = false
  task_family           = "quest_task"
  execution_role_arn    = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/questTaskRole"
  cloudwatch_group_name = aws_cloudwatch_log_group.cw_log_group.name
  lb_subnet_ids         = module.infra.outputs["public_subnets"]  #[for subnets in aws_subnet.public : subnets.id]
  ecs_subnet_ids        = module.infra.outputs["private_subnets"] #[for subnets in aws_subnet.private : subnets.id]
  container_name        = "quest"                                 # this is used by the load balancer
  container_definitions = [
    {
      cpu       = 512
      memory    = 1024
      essential = true
      image     = local.container_image
      name      = "quest"
      porotocol = "tcp"
      portMappings = [
        {
          containerPort = local.port_number
          hostPort      = local.port_number
      }]
      environment = [
        { name = "SOME_ENV_VAR", value = "SOME_VALUE" },
      ]
      secrets = [
        { name = "SECRET_WORD", valueFrom = "/quest/secret" },
      ]
      logConfiguration = {
        logDriver = "awslogs"
        options = {
          awslogs-group         = aws_cloudwatch_log_group.cw_log_group.name
          awslogs-region        = "us-east-1"
          awslogs-stream-prefix = "quest"
        }
      }
    },
  ]
  target_group_name = "quest"
  container_port    = local.port_number
  listener_port     = 443
  listener_protocol = "HTTPS"
  certificate_arn   = module.infra.outputs["certificate_arn"] #aws_acm_certificate.this.arn
  sg_rules = [
    {
      description     = "Egress rule for ECS SC"
      type            = "egress"
      from_port       = 0
      to_port         = 0
      protocol        = "-1"
      cidr_blocks     = ["0.0.0.0/0"]
      security_groups = []
    },
    {
      description     = "Ingress rule 8080 for ECS"
      type            = "ingress"
      from_port       = local.port_number
      to_port         = local.port_number
      protocol        = "tcp"
      cidr_blocks     = ["0.0.0.0/0"]
      security_groups = []
    },
    {
      description     = "Ingress rule for 443 ECS"
      type            = "ingress"
      from_port       = 443
      to_port         = 443
      protocol        = "tcp"
      cidr_blocks     = ["0.0.0.0/0"]
      security_groups = []
    },
  ]
  depends_on = [
    module.infra
  ]
}

resource "aws_iam_role" "quest" {
  name        = "questTaskRole"
  description = "Allows ECS tasks to call AWS services on your behalf."
  assume_role_policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Sid" : "",
        "Effect" : "Allow",
        "Principal" : {
          "Service" : "ecs-tasks.amazonaws.com"
        },
        "Action" : "sts:AssumeRole"
      }
    ]
  })
  managed_policy_arns = [
    "arn:aws:iam::aws:policy/AmazonSSMReadOnlyAccess",
    "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
  ]
  tags = { "Name" = "questTaskRole" }
}

output "ecs" {
  value = module.quest.ecs
}
