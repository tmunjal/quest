module "infra" {
  source          = "../modules/infra"
  vpc_cidr_block  = local.vpc_cidr_block
  public_subnets  = local.public_subnets
  private_subnets = local.private_subnets
}
