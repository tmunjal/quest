locals {
  port_number     = 3000
  container_image = "tarunmunjal/quest:latest"
  vpc_cidr_block  = "172.20.0.0/16"
  private_subnets = {
    subnet1 = {
      cidr = "172.20.32.0/20"
      az   = "us-east-1a"
      name = "private-a"
    }
    subnet2 = {
      cidr = "172.20.48.0/20"
      az   = "us-east-1b"
      name = "private-b"
    }
  }
  public_subnets = {
    subnet1 = {
      cidr = "172.20.0.0/20"
      az   = "us-east-1a"
      name = "public-a"
    }
    subnet2 = {
      cidr = "172.20.16.0/20"
      az   = "us-east-1b"
      name = "public-b"
    }
  }
}
