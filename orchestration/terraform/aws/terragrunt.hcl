
remote_state {
  backend = "s3"
  config = {
    region = "us-east-1"
    dynamodb_table = "terraform-state-lock"
    bucket = "${get_aws_account_id()}-terraform-state"
    # Path to Terraform state file within the bucket
    key = "quest/${path_relative_to_include()}/terraform_state.json"
    encrypt = true
  }
}

terraform {
#  before_hook "bucket_check" {
#    commands = ["init"]
#    execute = ["/usr/bin/python3", "../../../scripts/state_bucket.py"]    
#  }
#  before_hook "dynamo_table" {
#    commands = ["init"]
#  execute = ["/usr/bin/python3", "../../../scripts/tf_dynamo_table.py"]
#    
#  }
    extra_arguments "lock_retry" {
        commands  = get_terraform_commands_that_need_locking()

        arguments = [
        # Wait for up to 10 minutes to obtain the lock
        "-lock-timeout=10m"
        ]
    }

    extra_arguments "init_args" {
        commands = [
            "init"
        ]

    arguments = [
      # Disable the standard "colorization" of output
      "-no-color"
    ]
  }
}


generate "providers" {
    path = "_providers.tf"
    if_exists = "overwrite_terragrunt"
    
    contents = <<EOF
    terraform {
      backend "s3" {}
    }
    provider "aws" {
        region              = "us-east-1"
    }
EOF    
}
