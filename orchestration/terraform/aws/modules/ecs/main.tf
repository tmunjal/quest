resource "aws_ecs_cluster" "cluster" {
  name = var.ecs_cluster_name
  setting {
    name  = "containerInsights"
    value = "enabled"
  }
  tags     = merge(local.common_tags, var.tags)
  tags_all = merge(local.common_tags, var.tags)
}



resource "aws_ecs_service" "ecs_service" {
  name            = var.ecs_service_name
  cluster         = aws_ecs_cluster.cluster.id
  task_definition = aws_ecs_task_definition.task_definition.arn
  desired_count   = var.app_count
  launch_type     = "FARGATE"

  network_configuration {
    assign_public_ip = false
    security_groups  = [aws_security_group.sg.id]
    subnets          = var.ecs_subnet_ids
  }
  load_balancer {
    target_group_arn = aws_lb_target_group.target_group.arn
    container_name   = var.container_name
    container_port   = var.container_port
  }
  tags     = merge(local.common_tags, var.tags)
  tags_all = merge(local.common_tags, var.tags)

}
resource "aws_ecs_task_definition" "task_definition" {
  family                   = var.task_family
  network_mode             = "awsvpc"    ## not using from var right now
  requires_compatibilities = ["FARGATE"] ## not using from var right now
  cpu                      = var.task_cpu
  memory                   = var.task_memory
  task_role_arn            = var.task_role_arn
  execution_role_arn       = var.execution_role_arn
  container_definitions    = jsonencode(var.container_definitions)
  runtime_platform {
    operating_system_family = "LINUX"
  }
  tags     = merge(local.common_tags, var.tags)
  tags_all = merge(local.common_tags, var.tags)

}
