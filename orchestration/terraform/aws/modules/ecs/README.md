# `ecs` Module

This module creates ecs cluster, ecs service and ecs task definition with container definition. It creates a dns crecord as well as provisions an alb to route traffic to the provisined container. The logs are stored in cloudwatch.

## Requirements

## E.g. Inputs (Note: not all are specified due to time limit.)

| Name | #Type | #Defaults | #Description | 
| :--- | :--- | :--- | :--- |
| `tags`                          | map(string)  |null         | Tags you would like to add|

### Required Inputs

## Outputs
None

## Example Usage
```terraform
module "quest" {x
  vpc_id                = aws_vpc.main.id
  ecs_cluster_name      = "quest"
  ecs_service_name      = "quest"
  source                = "./modules/ecs"
  alb_name              = "quest-ecs"
  alb_internal          = false
  task_family           = "quest_task"
  execution_role_arn    = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/<SOMEROLE>"
  cloudwatch_group_name = aws_cloudwatch_log_group.cw_log_group.name
  lb_subnet_ids         = [for subnets in aws_subnet.public : subnets.id]
  ecs_subnet_ids        = [for subnets in aws_subnet.private : subnets.id]
  container_definitions = [
    {
      cpu       = 512
      memory    = 1024
      essential = true
      image     = "tarunmunjal/quest:latest"
      name      = "quest"
      porotocol = "tcp"
      portMappings = [
        {
          containerPort = local.port_number
          hostPort      = local.port_number
      }]
      environment = [
        { name = "SOME_ENV_VAR", value = "SOME_VALUE" },
      ]
      secrets = [
        { name = "SECRET_WORD", valueFrom = "/quest/secret" },
      ]
      logConfiguration = {
        logDriver = "awslogs"
        options = {
          awslogs-group         = aws_cloudwatch_log_group.cw_log_group.name
          awslogs-region        = "us-east-1"
          awslogs-stream-prefix = "quest"
        }
      }
    },
  ]
  target_group_name = "quest"
  container_port    = local.port_number
  listener_port     = 443
  listener_protocol = "HTTPS"
  certificate_arn   = aws_acm_certificate.this.arn
  sg_rules = [
    {
      description     = "Egress rule for ECS SC"
      type            = "egress"
      from_port       = 0
      to_port         = 0
      protocol        = "-1"
      cidr_blocks     = ["0.0.0.0/0"]
      security_groups = []
    },
    {
      description     = "Ingress rule 8080 for ECS"
      type            = "ingress"
      from_port       = local.port_number
      to_port         = local.port_number
      protocol        = "tcp"
      cidr_blocks     = ["0.0.0.0/0"]
      security_groups = []
    },
    {
      description     = "Ingress rule for 443 ECS"
      type            = "ingress"
      from_port       = 443
      to_port         = 443
      protocol        = "tcp"
      cidr_blocks     = ["0.0.0.0/0"]
      security_groups = []
    },
  ]
}

```


