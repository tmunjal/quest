resource "aws_security_group" "sg" {
  name        = var.ecs_service_name
  description = "Security group for access to and from gatekeeper ecs."
  vpc_id      = var.vpc_id

  tags     = merge(local.common_tags, var.tags, { "Name" = var.ecs_service_name })
  tags_all = merge(local.common_tags, var.tags, { "Name" = var.ecs_service_name })
}

resource "aws_security_group_rule" "rules" {
  count             = length(var.sg_rules)
  security_group_id = aws_security_group.sg.id
  description       = "${var.ecs_service_name}/${aws_security_group.sg.id} - cidr_rule ${count.index}"
  type              = var.sg_rules[count.index].type
  from_port         = var.sg_rules[count.index].from_port
  to_port           = var.sg_rules[count.index].to_port
  protocol          = var.sg_rules[count.index].protocol
  cidr_blocks       = var.sg_rules[count.index].cidr_blocks
  depends_on = [
    aws_security_group.sg
  ]
}

resource "aws_security_group_rule" "non_cider_rules" {
  count                    = length(var.non_cidr_rules)
  security_group_id        = aws_security_group.sg.id
  description              = "${var.ecs_service_name}/${aws_security_group.sg.id} - non_cidr_rule ${count.index}"
  type                     = var.non_cidr_rules[count.index].type
  from_port                = var.non_cidr_rules[count.index].from_port
  to_port                  = var.non_cidr_rules[count.index].to_port
  protocol                 = var.non_cidr_rules[count.index].protocol
  source_security_group_id = var.non_cidr_rules[count.index].source_security_group_id
  depends_on = [
    aws_security_group.sg
  ]
}
