resource "aws_lb" "ecs_lb" {
  name            = var.alb_name == null ? replace(var.ecs_service_name, "_", "-") : var.alb_name
  internal        = var.alb_internal
  subnets         = var.lb_subnet_ids
  security_groups = [aws_security_group.sg.id]
  tags            = merge(local.common_tags, var.tags)
  tags_all        = merge(local.common_tags, var.tags)
}

resource "aws_lb_target_group" "target_group" {
  name        = var.target_group_name
  port        = var.container_port
  protocol    = "HTTP"
  vpc_id      = var.vpc_id
  target_type = var.alb_target_type
  health_check {
    path = var.tg_health_check_path
  }
  tags     = merge(local.common_tags, var.tags)
  tags_all = merge(local.common_tags, var.tags)

}

resource "aws_lb_listener" "lb_listener" {
  port     = var.listener_port
  protocol = var.listener_protocol
  # If protocol isn't HTTPS then ssl_policy and certificate_arn cannot be part of the listener.
  ssl_policy        = var.listener_protocol != "HTTPS" ? null : var.ssl_policy
  certificate_arn   = var.listener_protocol != "HTTPS" ? null : var.certificate_arn
  load_balancer_arn = aws_lb.ecs_lb.arn
  default_action {
    target_group_arn = aws_lb_target_group.target_group.id
    type             = var.listener_default_action_type
  }
  tags     = merge(local.common_tags, var.tags)
  tags_all = merge(local.common_tags, var.tags)
}

