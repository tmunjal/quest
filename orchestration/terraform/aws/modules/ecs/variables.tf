### Used in alb.tf
## ALB variables.
variable "vpc_id" {
  type        = string
  default     = null
  description = "VPC to put ecs service in"
}

variable "alb_name" {
  type        = string
  default     = null
  description = "A name for the alb that will be created for routing traffic to the ecs deployment."
}

variable "lb_subnet_ids" {
  type        = list(any)
  default     = []
  description = "Subnets to use for ecs lb"
}

variable "target_group_name" {
  type        = string
  default     = null
  description = "Target group name."

}

variable "alb_target_type" {
  type        = string
  default     = "ip"
  description = "Alb target type."
}

variable "tg_health_check_path" {
  type        = string
  default     = "/"
  description = "path to check for the health check for ecs deployment."
}

variable "listener_port" {
  type        = string
  default     = "80"
  description = "Port that will be assigned to the listener for external access. Can be different from container_port"

}
variable "listener_protocol" {
  type        = string
  default     = "HTTP"
  description = "ALB listener protocol HTTP/HTTPS"
}

variable "ssl_policy" {
  type        = string
  default     = ""
  description = "Listener ssl policy."
}

variable "certificate_arn" {
  type        = string
  default     = ""
  description = "Certificate arn that will be used for alb."
}

variable "listener_default_action_type" {
  type        = string
  default     = "forward"
  description = "ALB listener default action. Default is set to forwrad requests to target group."
}

variable "alb_dns" {
  type        = string
  default     = null
  description = "ALB DNS name"
}

variable "alb_internal" {
  type        = bool
  default     = false
  description = "ALB DNS name"
}

variable "route53_zone" {
  type        = string
  default     = null
  description = "Primary zone for dns entries"
}
## ECS Variables

variable "ecs_cluster_name" {
  type        = string
  default     = null
  description = "ECS cluster name"
}

variable "ecs_service_name" {
  type        = string
  default     = null
  description = "ECS Service name"
}

variable "app_count" {
  type        = string
  default     = 1
  description = "App count"
}


variable "target_groups" {
  type        = list(any)
  default     = []
  description = "Target group and listener information."
}

variable "non_cidr_rules" {
  type        = list(any)
  default     = []
  description = "(optional) non cidr rule e.g. sg as source"
}
variable "sg_rules" {
  type = list(any)
  default = [
    {
      description = "Egress rule for ECS SC"
      type        = "egress"
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      description = "Ingress rule for ECS SC"
      type        = "ingress"
      from_port   = -1
      to_port     = -1
      protocol    = "443"
      cidr_blocks = ["0.0.0.0/0"]
    },
  ]
}

variable "tags" {
  type = map(string)
  default = {
    "ManagedByModule" = "marketplace/modules/ecs"
  }
  description = "Additional tags for ecs module"
}

variable "task_family" {
  default     = null
  type        = string
  description = "Task definition name"
}

variable "task_cpu" {
  default     = 1024
  type        = number
  description = "Task cpu."
}

variable "task_memory" {
  default     = 2048
  type        = number
  description = "Task memory."
}


variable "task_role_arn" {
  default     = null
  type        = string
  description = "ARN for the role that the task will require. This is for the service to be able manage AWS Resource it needs to."
}

variable "execution_role_arn" {
  default     = null
  type        = string
  description = "Task execution role. An already managed role 'arn:aws:iam::<AWS_ACCOUNT_ID>:role/ecsTaskExecutionRole' exists and can be used. This is required if you want to get ECR images."
}

variable "container_port" {
  default     = 3000
  type        = number
  description = "Container's port. This is used in load_balancer configuration section of ecs service resource."
}

variable "container_name" {
  default     = "quest"
  type        = string
  description = "Container's name. This is used in load_balancer configuration section of ecs service resource."
}

variable "cloudwatch_group" {
  type        = string
  default     = null
  description = "cloudwatch group name"
}


variable "capacity_provider_type" {
  type        = list(string)
  default     = ["FARGATE_SPOT"]
  description = "FARGATE_SPOT or FARGATE. Defaults to FARGATE_SPOT"

}

variable "ecs_subnet_ids" {
  type        = list(any)
  default     = []
  description = "Subnets to use for ecs"

}

variable "hosted_zone_id" {
  type        = string
  default     = ""
  description = "Hosted zone where dns record will be created"
}

variable "container_definitions" {
  type        = any
  description = "container definitions"
}

variable "cloudwatch_group_name" {
  type        = string
  description = "Cloudwatch group information where container will send logs to."
}
