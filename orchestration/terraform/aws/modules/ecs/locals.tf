locals {
  ## Common tags that will be used by the module. More tags can be added but updating variable common_tags.
  common_tags = {
    ManagedByModule = "modules/ecs"
  }
}
