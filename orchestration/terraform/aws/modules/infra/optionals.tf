
resource "aws_route53_zone" "private" {
  name = "mytesting.com"

  vpc {
    vpc_id = aws_vpc.main.id
  }
}

