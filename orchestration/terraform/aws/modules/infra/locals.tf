locals {
  ## Common tags that will be used by the module. More tags can be added but updating variable common_tags.
  common_tags = {
    ManagedByModule = "Some common Tag"
  }
  first_public_subnet = keys(var.public_subnets)[0]
}

