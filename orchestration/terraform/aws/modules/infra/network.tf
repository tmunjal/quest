## VPC
resource "aws_vpc" "main" {
  cidr_block       = var.vpc_cidr_block
  instance_tenancy = "default"

  tags = merge(local.common_tags, {
    Name = "main"
  })
}

## Internet Gateway
resource "aws_internet_gateway" "this" {
  vpc_id = aws_vpc.main.id

  tags = merge(local.common_tags, {
    Name = "default ingernet gateway"
  })
}

## Public subnets

resource "aws_subnet" "public" {
  for_each          = var.public_subnets
  vpc_id            = aws_vpc.main.id
  cidr_block        = each.value["cidr"]
  availability_zone = each.value["az"]
  tags = merge(local.common_tags, {
    Name = each.value["name"]
  })
}

## Public route table 

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.main.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.this.id
  }
  tags = merge(local.common_tags, {
    Name = "Internet GW to allow internet access for private subnets"
  })
}

## Public route table association
resource "aws_route_table_association" "public_subnet" {
  for_each       = toset(keys(var.public_subnets))
  subnet_id      = aws_subnet.public[each.key].id
  route_table_id = aws_route_table.public.id
}

### private

## Private subnets with NAT Gateway

## private subnets
resource "aws_subnet" "private" {
  for_each          = var.private_subnets
  vpc_id            = aws_vpc.main.id
  cidr_block        = each.value["cidr"]
  availability_zone = each.value["az"]
  tags = merge(local.common_tags, {
    Name = each.value["name"]
  })
}

## Elastic IP allocation for NAT Gateway
resource "aws_eip" "nat-gw-ip" {
  depends_on = [
    aws_route_table_association.public_subnet
  ]
  vpc = true
}

## NAT Gateway
resource "aws_nat_gateway" "this" {
  depends_on = [
    aws_eip.nat-gw-ip
  ]

  # Allocating the Elastic IP to the NAT Gateway!
  allocation_id = aws_eip.nat-gw-ip.id

  # Associating it in the Public Subnet!
  subnet_id = aws_subnet.public["${local.first_public_subnet}"].id
  tags = {
    Name = "Nat-Gateway_Project"
  }
}

# Creating a Route Table for the Nat Gateway!
resource "aws_route_table" "nat-gw" {
  depends_on = [
    aws_nat_gateway.this
  ]

  vpc_id = aws_vpc.main.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.this.id
  }

  tags = {
    Name = "Route Table for NAT Gateway"
  }

}

## Route table association for private subnet
resource "aws_route_table_association" "private_subnet" {
  for_each       = toset(keys(var.private_subnets))
  subnet_id      = aws_subnet.private[each.key].id
  route_table_id = aws_route_table.nat-gw.id
}




