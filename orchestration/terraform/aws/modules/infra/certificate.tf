resource "aws_acm_certificate" "this" {
  private_key      = file("../../../../scripts/privateKey.pem")
  certificate_body = file("../../../../scripts/certificateChain.pem")
  lifecycle {
    ignore_changes = all
  }
}
