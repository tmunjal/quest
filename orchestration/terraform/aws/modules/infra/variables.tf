## Used in vpc.tf
variable "vpc_cidr_block" {
  default     = "172.20.0.0/16"
  description = "default vpc cidr block"
}

## Used in subnets.tf
variable "public_subnets" {
  type = map(any)
  default = {
    subnet1 = {
      cidr = "172.20.0.0/20"
      az   = "us-east-1a"
      name = "public-a"
    }
    subnet2 = {
      cidr = "172.20.16.0/20"
      az   = "us-east-1b"
      name = "public-b"
    }
  }
  description = "A map of subnets to be created"
}

variable "private_subnets" {
  type = map(any)
  default = {
    subnet1 = {
      cidr = "172.20.32.0/20"
      az   = "us-east-1a"
      name = "private-a"
    }
    subnet2 = {
      cidr = "172.20.48.0/20"
      az   = "us-east-1b"
      name = "private-b"
    }
  }
  description = "A map of subnets to be created"
}

variable "image_tag" {
  type        = string
  default     = ""
  description = "Subnets to use for ecs lb"
}


## OPTIONAL Variables for extra functionality
