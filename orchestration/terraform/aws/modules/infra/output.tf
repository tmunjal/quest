output "outputs" {
  value = {
    "public_subnets" : [for subnets in aws_subnet.public : subnets.id],
    "private_subnets" : [for subnets in aws_subnet.private : subnets.id],
    "certificate_arn" : aws_acm_certificate.this.arn,
    "vpc_id" : aws_vpc.main.id
  }
}

