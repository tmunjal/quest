# `infra` Module

This module is used to setup a new aws account. The module will setup the following resources.
1. VPC
2. Public Subnets
3. Private Subnets
4. Internet Gateway
5. NAT Gateway
6. Public Route table
7. Private Route table (with NAT Gateway.)
8. Route table association with NAT to private subnets
9. Route table association with IGW to public subnets
10. ACM self signed Certificate

## Requirements

## Inputs

| Name | #Type | #Defaults | #Description | 
| :--- | :--- | :--- | :--- |
| `vpc_cidr_block`                          | map(string)   |null           | CIDR block that will be assigned to the VPC.|
| `public_subnets`                          | map(any)      |null           | Subnet information including range from VPC CIDR Range|
| `private_subnets`                         | map(any)      |null           | Subnet information including range from VPC CIDR Range|
### Required Inputs

## Outputs


"outputs": {
    "public_subnets" : ["public subnet ids"],
    "private_subnets" : ["private subnet ids"],,
    "certificate_arn" : "certificate arn",
    "vpc_id" : "vpc id"
}


## Example Usage
```terraform
module "infra" {
vpc_cidr_block = "172.20.0.0/16"
  
public_subnets = {
    subnet1 = {
      cidr = "172.20.0.0/20"
      az   = "us-east-1a"
      name = "public-a"
    }
    subnet2 = {
      cidr = "172.20.16.0/20"
      az   = "us-east-1b"
      name = "public-b"
    }
}

private_subnets = {
    subnet1 = {
      cidr = "172.20.32.0/20"
      az   = "us-east-1a"
      name = "private-a"
    }
    subnet2 = {
      cidr = "172.20.48.0/20"
      az   = "us-east-1b"
      name = "private-b"
  }
}

```


