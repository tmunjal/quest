# Use an official Node.js runtime as a parent image
## Ideally this should be a base image that will be in a private registry for security purposes.
FROM node:16-alpine

# Set the working directory to /app
WORKDIR /app

# Copy package.json and package-lock.json to the container
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy the rest of the application code to the container
COPY . .

# Expose port 3000
EXPOSE 3000

# Define the command that starts the app
CMD [ "npm", "start" ]
