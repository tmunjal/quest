#!/usr/bin/env python3
############################################################################
# This script handles setting up the DynamoDB table that Terraform will use
# for state locking. It is idempotent, in that if the bucket already exists
# it will not do anything.
############################################################################

import boto3

REGION = "us-east-1"
TABLE_NAME = "terraform-state-lock"


def table_exists(dynamodb_client) -> bool:
    """
    Tests to see if the table already exists in the account.  Note that tables
    only have to be unique within a region, so the dynamodb_client needs to
    have a specific region attached.
    :param dynamodb_client: boto3 DynamoDB client
    :return: True if the table exists, False if not
    """
    try:
        dynamodb_client.describe_table(TableName=TABLE_NAME)
        return True
    except dynamodb_client.exceptions.ResourceNotFoundException:
        return False


def run() -> None:
    """
    Tests to see if the table exists and creates it if it does not.
    :return: None
    """
    dynamodb_client = boto3.client('dynamodb', region_name=REGION)

    if table_exists(dynamodb_client):
        print(f"{TABLE_NAME} table already exists")
        return

    print(f"Creating {TABLE_NAME} table")

    dynamodb_client.create_table(
        TableName=TABLE_NAME,
        AttributeDefinitions=[
            {
                'AttributeName': 'LockID',
                'AttributeType': 'S'
            },
        ],
        KeySchema=[
            {
                'AttributeName': 'LockID',
                'KeyType': 'HASH'
            },
        ],
        BillingMode="PAY_PER_REQUEST"
    )

    print(f"Table {TABLE_NAME} created")


if __name__ == "__main__":
    run()