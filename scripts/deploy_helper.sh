#!/bin/bash
############################################################################
# This is a helper script that can be used locally for build and deployment.
############################################################################


# Reset
Color_Off='\033[0m'       # Text Reset

# Regular Colors
BLACK='\033[0;30m'        # Black
RED='\033[0;31m'          # Red
GREEN='\033[0;32m'        # Green
YELLOW='\033[0;33m'       # Yellow
BLUE='\033[0;34m'         # Blue
PURPLE='\033[0;35m'       # Purple
CYAN='\033[0;36m'         # Cyan
WHITE='\033[0;37m'        # White

SCRIPT_PATH=$(readlink -f $0)

function generate_cert {
    cd $(dirname ${SCRIPT_PATH})
    openssl req -x509 -newkey rsa:2048 -keyout privateKey.pem -out certificateChain.pem -days 365 -nodes -subj '/C=US/ST=New York/L=New York City/O=tarun/OU=munjal/CN=*.mytesting.com'
}

function build {
    if [[ -z $DOCKER_REGISTRY ]]; then
        DOCKER_REGISTRY="docker.io"
    fi
    if [[ -z $DOCKER_REGISTRY_IMAGE ]]; then
        DOCKER_REGISTRY_IMAGE="index.docker.io/tarunmunjal"
    fi
    if [[ -z $DOCKER_REGISTRY_USER ]]; then
        echo "DOCKER_REGISTRY_USER is required for pushing image."
    fi
    if [[ -z $DOCKER_REGISTRY_PASSWORD ]]; then
        echo "DOCKER_REGISTRY_PASSWORD is required for pushing image."
    fi
    if [[ -z $DOCKER_REPO ]]; then
        echo "DOCKER_REPO not specified setting it to quest."
        DOCKER_REPO="quest"
    fi
    if [[ -z $DOCKER_FILE_PATH ]]; then
        echo "DOCKER_FILE_PATH not specified setting it to Dockerfile."
        DOCKER_FILE_PATH="Dockerfile"
    fi
    if [[ -z $CI_COMMIT_SHORT_SHA ]]; then
        CI_COMMIT_SHORT_SHA=$(git rev-parse --short HEAD)
    fi
    DOCKER_LOGIN_RESULT=$(docker login -u $DOCKER_REGISTRY_USER -p $DOCKER_REGISTRY_PASSWORD >&2 && echo "SUCCESS" || echo "FAILED")
    cd $(dirname $(dirname ${SCRIPT_PATH}))
    REGISTRY_PATH="${DOCKER_REGISTRY_IMAGE}/$DOCKER_REPO"
    docker build -f $DOCKER_FILE_PATH -t "${REGISTRY_PATH}:${CI_COMMIT_SHORT_SHA}" .
    docker tag "${REGISTRY_PATH}:${CI_COMMIT_SHORT_SHA}" "${REGISTRY_PATH}:latest"
    if [[ $DOCKER_LOGIN_RESULT == "SUCCESS" ]]; then
        echo "docker push \"${REGISTRY_PATH}:${CI_COMMIT_SHORT_SHA}\"; docker push \"${REGISTRY_PATH}:latest\""
        #docker push "${REGISTRY_PATH}:${CI_COMMIT_SHORT_SHA}"; docker push "${REGISTRY_PATH}:latest"
    else
        echo "Docker login failed. The image will not be pushed. Use the following command to push the image(s)"
        echo "docker push \"${REGISTRY_PATH}:${CI_COMMIT_SHORT_SHA}\"; docker push \"${REGISTRY_PATH}:latest\""
        exit 1
    fi
}

function terragrunt_plan () {
    generate_cert
    cd $(dirname $(dirname ${SCRIPT_PATH}));
    ./scripts/state_bucket.py
    ./scripts/tf_dynamo_table.py
    cd orchestration/terraform/aws/test-account
    terragrunt plan
}

function terragrunt_apply () {
    generate_cert
    cd $(dirname $(dirname ${SCRIPT_PATH}));
    ./scripts/state_bucket.py
    ./scripts/tf_dynamo_table.py
    cd $(dirname $(dirname ${SCRIPT_PATH}))
    cd orchestration/terraform/aws/test-account
    if [[ -z $CI_BUILD_ID ]]; then
        terragrunt apply
    else
        terragrunt apply --auto-approve
    fi
}

function terragrunt_destroy () {
    cd $(dirname $(dirname ${SCRIPT_PATH}))
    cd orchestration/terraform/aws/test-account
    if [[ -z $CI_BUILD_ID ]]; then
        terragrunt destroy
    else
        terragrunt destroy --auto-approve
    fi
    export BUCKET_NAME="terraform-state-${AWS_ACCOUNT_ID}"
    aws s3api delete-objects --bucket ${BUCKET_NAME} --delete "$(aws s3api list-object-versions --bucket ${BUCKET_NAME} --query='{Objects: Versions[].{Key:Key,VersionId:VersionId}}')" || true
    aws s3api delete-objects --bucket ${BUCKET_NAME} --delete "$(aws s3api list-object-versions --bucket ${BUCKET_NAME} --query='{Objects: DeleteMarkers[].{Key:Key,VersionId:VersionId}}')" || true
    aws s3api delete-bucket --bucket $BUCKET_NAME
    aws dynamodb delete-table --table-name "terraform-state-lock"
}

function run () {
    echo $(dirname $(dirname ${SCRIPT_PATH}))
}
function parse_args () {
    while [ $# -gt 0 ];
    do
        case "$1" in 
            --*) 
                VAR_NAME=${1%=*}
                VAR_NAME=$(echo ${VAR_NAME#*--} | tr '[:lower:]' '[:upper:]')
                VAR_VALUE=${1#*=*}
                ;;
            *)
                VAR_VALUE="${VAR_VALUE} ${1}"
                ;;
        esac
        if [[ ! -z $VAR_NAME ]]; then
            declare -g "$VAR_NAME"="$VAR_VALUE"``
        fi
        shift
    done
}

function help () {
    echo -e ${YELLOW}"Please choose one of the following commands and execute from the root of the repo" ${Color_Off}
    printf "Build quest image                           : "
    printf ${CYAN}"script/deploy_helper build --docker_file_path=orchestration/images/Dockerfile --docker_repo=<your repo>"${Color_Off}"\n"
    printf "Build base image                            : "
    printf ${CYAN}"script/deploy_helper build --docker_file_path=orchestration/images/Dockerfile --docker_repo=<your repo>"${Color_Off}"\n"
    printf "See what infra would be provisioned Execute : "
    printf ${CYAN}"script/deploy_helper terragrunt_plan"${Color_Off}"\n"
    printf "Provision infrastructure Execute:           : "
    printf ${CYAN}"script/deploy_helper terragrunt_apply"${Color_Off}"\n"
    printf "Destroy provisioned infrastrucutre Execute  : "
    printf ${CYAN}"script/deploy_helper terragrunt_destroy"${Color_Off}"\n"
}
parse_args ${@:1}
## check if we have aws credentials
AWS_ACCOUNT_ID=$(aws sts get-caller-identity --query 'Account' --output text) || exit
$@
