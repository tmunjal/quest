#!/usr/bin/env python3
############################################################################
# This script handles setting up the bucket that Terraform will use for
# remote state.  It is idempotent, in that if the bucket already exists
# it will not do anything.
############################################################################

import boto3
from botocore.client import ClientError
import time

# Region in which the bucket will be created.  If this gets changed to anything
# other than "us-east-1", then a LocationConstraint must be added to the
# create_bucket call.
REGION = "us-east-1"


def bucket_exists(s3_client, bucket_name: str) -> bool:
    """
    Tests to see if an S3 bucket exists
    :param s3_client: boto3 s3 client
    :param bucket_name: Name of the bucket to check
    :return: True if the bucket exists, False if it does not
    """
    try:
        s3_client.head_bucket(Bucket=bucket_name)
        return True
    except ClientError:
        return False


def run() -> None:
    """
    Tests to see if the Terraform bucket exists in the current account and creates it
    if it does not.
    :return: None
    """
    # Get the Account ID for the current account
    sts_client = boto3.client("sts", region_name=REGION)
    account_id: str = sts_client.get_caller_identity()["Account"]

    # Setting bucket name to "terraform-state-<aws_account_id>" this will keep the bucket unique.
    # This will also ensure that this script can be run in any aws account.
    bucket_name = f"terraform-state-{account_id}"
    s3_client = boto3.client("s3", region_name=REGION)

    # If the bucket already exists, we're done
    if bucket_exists(s3_client, bucket_name):
        print(f"{bucket_name} already exists")
        return

    print(f"Creating bucket {bucket_name}")

    # Note - explicitly omitting the LocationConstraint parameter causes the bucket
    # to be created in us-east-1.
    s3_client.create_bucket(
        Bucket=bucket_name,
        ACL="private",
    )

    # A bucket may not actually become available until some seconds after the
    # API above returns
    while not bucket_exists(s3_client, bucket_name):
        print("Waiting for {bucket_name} to be ready")
        time.sleep(1)

    # AES256 encryption on the bucket using the default S3 key
    s3_client.put_bucket_encryption(
        Bucket=bucket_name,
        ServerSideEncryptionConfiguration={
            "Rules": [
                {
                    "ApplyServerSideEncryptionByDefault": {"SSEAlgorithm": "AES256"}
                }
            ]
        }
    )

    # Turn on versioning on the bucket
    s3_client.put_bucket_versioning(
        Bucket=bucket_name,
        VersioningConfiguration={
            "Status": "Enabled",
        }
    )

    # Disable public access to the bucket
    s3_client.put_public_access_block(
        Bucket=bucket_name,
        PublicAccessBlockConfiguration={
            "BlockPublicAcls": True,
            "IgnorePublicAcls": True,
            "BlockPublicPolicy": True,
            "RestrictPublicBuckets": True
        }
    )

    print(f"The {bucket_name} bucket is ready for Terraform use")


if __name__ == "__main__":
    run()