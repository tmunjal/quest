# Prerequisites
    * Terraform
    * Terragrunt
    * awscli
    * aws credentials that has permissions to create/update/destroy: 
     - vpc 
     - subnets  
     - internet gateway
     - nat gateway
     - route tables
     - route table associations
     - alb
     - secrets
     - target groups
     - ecs fargate cluster/service/task
     - security groups
     - acm 
     - route53 zone
    * docker hub repos (one for quest another for base that will have the tools needed to run the gitlab-ci. e.g. terraform; terragrunt & aws cli)
NOTE: If you keep your dockerhub repo private then you will need to setup AWS secret and add that to the role that is attached to the ecs task so the image can be pulled. For this test I have used a publically accessible repo in my dockerhub account `tarunmunjal/quest`

NOTE: For testing you can just setup AWS environment variables locally and go to orchestration/terraform/aws/test-account and execute `terragrunt init; terragrunt apply --auto-approve` and at the end it would spit out the load balancer url that you can access on https once dns is populated by aws. This can be done without changing anything. This would create an s3 bucket in the account that you plan to test this in.


# Why was the current solution choosen?
* The exercise is vague and can be interpreted and implemented nearly infinite ways.
* The idea seems to be to show case Automation; CI/CD; IaC; Docker; Containerization; Cloud Experience.
    * Created a modules for ecs to show the knowledge behind creating resusable code.
    * Used terragrunt to keep code DRY and reusable for any environment/aws accounts.
    * Set up CI/CD for image creation and deployment.
    * The code can be run as is by just providing AWS Credentials capable of creating the resources defined here. The code would output public load balancer dns name that can be used on port 443 to access the site.

# Why not setup route53 and a public domain and public cert.?
* Not specified. Seems out of it. As of this writing terraform also cannot request a domain from route53 aws cli however, can.

# Why not use private repository like ECR or dockerhub private?
* Time constraints.

More time and more details about the audience and the use case would be helpful in reaching an ideal state where the code can be freely used in any aws environment.


## How to deploy and test this code is working?
1. Use the provided gitlab-ci
    * Fork the repo to your own `gitlab.com` repo
    * Add the following Variables to your project's pipeline. Gitlab instructions can be found [here](https://docs.gitlab.com/ee/ci/variables/#for-a-project)
      ![Pipeline Variable screenshot](images/PipelineVariables.png)
    * Update variables in [locals.tf](orchestration/terraform/aws/test-account/locals.tf)
    * Execute the jobs manually in order
        1. docker-build-base
        2. docker-build-quest
        3. terragrunt-plan
        4. terragrunt-apply
        5. terragrunt-destroy    
2. Using the script to execute locally. NOTE: 
    * cd to root of the repo.
    * ./scripts/deploy_helper.sh --docker_repo=ubuntu --docker_file_path=Dockerfile # to build image and push it to the repo. 
    * Update image tag and other variables in [locals.tf](orchestration/terraform/aws/test-account/locals.tf).
    * scripts/deploy_helper.sh terragrunt_plan
    * scripts/deploy_helper.sh terragrunt_apply
    * scripts/deploy_helper.sh terragrunt_destroy
3. Locally
    * Create and push docker file to your repo. You will need to change the Image and tag appropirately according to your needs. A good strategy is to use git sha for all images to easily track what code/commit was used during the build process.
    * Push this to a repository that can be publically accessible.
    * Update image tag and other variables in [locals.tf](orchestration/terraform/aws/test-account/locals.tf).
    * cd to [test-account](orchestration/terraform/aws/test-account) folder and execute the following commands.
        1. terragrunt init
        2. terragrunt plan #(check the resources that will be created and ensure you understand these.)
        3. terragrunt apply #(if you want to setup the infrastrucutre for test then when prompted type yes and enter)
        4. Once apply finishes it will output the external url for the loadbalancer. You can use that to access the site on https.
        5. terragrunt destroy --auto-approve # Do this when you are done with your testing to destroy all the resources. 

## Note: Depending upon the approach you might be left with s3 bucket and dynamodb table that you will need to delete manually.


# How can this be improved?
* More time
* More defined criteria about what is available and what is required. (e.g. If AWS is pre-setup or a brand new?).
* The application as is can also be served from s3 bucket using cloudfront. But the checks would fail because then it won't be in docker.
* There is currently no autoscaling setup either.
* ECS was used since it's a single app and didn't require a heavy EKS cluster.
* Parmaeter Store is used for secret but if the secret is to be properly protected a kms key could be generated and used and then an iam role would be assigned to allow specific services to use the kms key to decrypt the value.
* No mention of monitoring or alerting here so, it hasn't been setup.

Submission of working screenshots.
![/aws](images/Capture_quest_aws.png)
![/loadbalanced](images/Capture_quest_loadbalanced.png)
![/secret_word](images/Capture_quest_secret_word.png)
![/tls](images/Capture_quest_tls.png)
![/docker](images/Capture_quest_docker.png)


## Please report any bugs and updates to documentation by creating a Merge Request.
